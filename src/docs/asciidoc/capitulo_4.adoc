////
    capitulo_4.adoc

    What are the tehtar and which flavours they have.
////


== Las [.elven-heading]##tehtar##

=== ¿Qué son las [.elven-heading]##tehtar##?

Las [.elven]##tehtar## son los símbolos élficos para representar los sonidos vocálicos. Hay dos métodos para representar los sonidos vocálicos: a través de las [.elven]##tehtar## propiamente dichas o usando [.elven]##tengwar## particulares para representarlas.

Personalmente prefiero el uso de [.elven]##tehtar## antes que de [.elven]##tengwar## para representar vocales, y esa será la filosofía que siga en este documento.

=== Vocales basadas en [.elven-heading]##tengwar##

Este modelo se basa en el uso de [.elven]##tengwar## que no tienen sonidos consonánticos asociados (están libres). La correspondencia de vocales con estas [.elven]##tengwar## es la siguiente.

.Sonidos vocálicos representados con [.elven]##tengwar##
[cols="3", options=header, stripes=odd, caption="Tabla {counter:table-number}: "]
|===

a|[.elven-heading]##Tengwa##
a|Nombre
a|Sonido

a|[.tengwar-in-table]##pass:[n]##
a|[.elven]##Wilya##
a|[.phoneme]##a##

a|[.tengwar-in-table]##pass:[l]##
a|[.elven]##Yanta##
a|[.phoneme]##e##

a|[.tengwar-in-table]##pass:[`]##
a|[.elven]##Telco##
a|[.phoneme]##i##

a|[.tengwar-in-table]##pass:[h]##
a|[.elven]##Anna##
a|[.phoneme]##o##

a|[.tengwar-in-table]##pass:[.]##
a|[.elven]##Úrë##
a|[.phoneme]##u/ü/w##

|===

=== Vocales basadas en [.elven-heading]##tehtar##

Estas son las [.elven]##tehtar## del modo de escritura élfica en español. Nótese que las [.elven]##tehtar## no tienen nombre propio, al contrario que las [.elven]##tengwar##.

.Sonidos vocálicos representados con [.elven]##tehtar##
[cols="2", options=header, stripes=odd, caption="Tabla {counter:table-number}: "]
|===

a|[.elven-heading]##Tehta##
a|Sonido

a|[.tengwar-inactive-in-table]##pass:[`]##[.tengwar-in-table]##pass:[C]##
a|[.phoneme]##a##

a|[.tengwar-inactive-in-table]##pass:[`]##[.tengwar-in-table]##pass:[V]##
a|[.phoneme]##e##

a|[.tengwar-inactive-in-table]##pass:[`]##[.tengwar-in-table]##pass:[B]##
a|[.phoneme]##i##

a|[.tengwar-inactive-in-table]##pass:[`]##[.tengwar-in-table]##pass:[N]##
a|[.phoneme]##o##

a|[.tengwar-inactive-in-table]##pass:[`]##[.tengwar-in-table]##pass:[M]##
a|[.phoneme]##u/ü/w##

|===

Cada tehta se muestra encima de la [.elven]##tengwa## [.elven]##Telco## ([.tengwar]##pass:[`]##), también llamada portadora corta. Esta [.elven]##tengwa## no tiene significado alguno, y solo aparece cuando la [.elven]##tehta## debe aparecer sola, como veremos al explicar las reglas de escritura.

Por poner un ejemplo, la sílaba «ma» se representaría con este sistema como [.tengwar]##pass:[t##]#.

En la tabla puede apreciarse que las [.elven]##tehtar## correspondientes a la «i» y a la «u» pueden representar diversas variantes vocálicas. Así, la [.elven]##tehta## para la «i» sirve para representar el sonido «i» (como en «v[.mark]##i##aje»), pero también para representar el sonido «y» homófono con «i» (serviría para «le[.mark]##y##», pero no para «Yeyo»).

De igual forma, la [.elven]##tehta## correspondiente a la «u» serviría para representar el sonido «u» (como en «b[.mark]##u##rro»), para el sonido «ü» (como en «Ag[.mark]##ü##imes») y para el sonido «w» cuando se trate de extranjerismos homófonos con la «u» (como en «[.mark]##W##inchester»).
